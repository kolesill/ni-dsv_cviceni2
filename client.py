import statistics_pb2_grpc
import statistics_pb2
import grpc


def read_words():
    while True:
        print("Write words to count:")
        msg = statistics_pb2.Message(body=input())
        yield msg


def send_words(stub):
    responses = stub.CountWords(read_words())
    for repsonse in responses:
        print(repsonse.body)


def run():
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = statistics_pb2_grpc.StatisticsStub(channel)
        send_words(stub)


if __name__ == '__main__':
    run()
