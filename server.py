from concurrent import futures
import grpc
import statistics_pb2_grpc
import statistics_pb2


def count_words(body):
    stat = dict()
    words = body.split()

    for word in words:
        if word in stat:
            stat[word] += 1
        else:
            stat[word] = 1

    return str(stat)


class StatisticsServicer(statistics_pb2_grpc.StatisticsServicer):

    def CountWords(self, request_iterator, context):
        for words in request_iterator:
            count = count_words(words.body)
            yield statistics_pb2.Message(body=count)


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    statistics_pb2_grpc.add_StatisticsServicer_to_server(
        StatisticsServicer(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    serve()
